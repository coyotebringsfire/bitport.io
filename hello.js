"use latest;"
var Twitter = require('twitter');

module.exports = function ({secrets, query}, cb) {
  console.log(secrets);
  var client = new Twitter({
    consumer_key: secrets.key,
    consumer_secret: secrets.secret,
    access_token_key: secrets.access_token_key,
    access_token_secret: secrets.access_token_secret
  });
  client.post('statuses/update', {status: `${query.timer} timer ended`},  function(error, tweet, response) {
    if(error) {
      return cb(error);
    }
    console.log(tweet);
    cb(null, tweet);
  });

}
